import React from 'react';
import './PreferenceEntry.css';

// One of possibility for one parameter
function PreferenceEntry(props) {

    let handleClick = () => {
        props.onClick()
    }

    return (
        <div className={`preference-entry${props.active ? " active" : ""}`} onClick={handleClick}>
            {props.children}
            <p>{props.preference.name}</p>
        </div>
    )
}

export default PreferenceEntry;