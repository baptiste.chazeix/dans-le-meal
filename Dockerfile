FROM node:alpine as build

WORKDIR /app
ENV PATH /app/node_modules/.bin:$PATH

COPY . /app/

# Prepare the container for building React
RUN npm install
RUN npm install react-scripts -g

# Build for production
RUN npm run build

# Prepare nginx
FROM nginx:alpine
COPY --from=build /app/build /usr/share/nginx/html
RUN rm /etc/nginx/conf.d/default.conf
COPY nginx.conf /etc/nginx/conf.d/react.conf
RUN rm -rf /app

# Add cerbot to use https
RUN apk add certbot certbot-nginx
RUN mkdir /etc/letsencrypt

# Launch up nginx
EXPOSE 80
EXPOSE 443
CMD ["nginx", "-g", "daemon off;"]
